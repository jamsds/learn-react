import axios from "axios";
import cookie from "react-cookies";

const Authentication = {
  isAuthenticated: false,

  authenticate(cb, username, password) {
    if (cookie.load("authentic_num")) {
      setTimeout(cb, 100);
    } else {
      axios
        .get(
          "http://mail.freewebmoa.co.kr:8087/api/getAuth?mobile_device=c&mobile_regid=ca&newKey=false&user_id=" +
            username +
            "&user_pass=" +
            password
        )
        .then(res => {
          if (res.data.authentic_num) {
            let d = new Date();
            d.setTime(d.getTime() + 30 * 60 * 1000);
            cookie.save("authentic_num", res.data.authentic_num, {
              path: "/",
              expires: d
            });
            setTimeout(cb, 100);
          } else {
            console.log(res.data);
          }
        })
        .catch(console.log);
    }
  }
};

export default Authentication;
