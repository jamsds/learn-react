import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import Authentication from "./authentication";

function Login() {
  let history = useHistory();
  let location = useLocation();

  let { from } = location.state || { from: { pathname: "/" } };
  let login = () => {
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;

    Authentication.authenticate(
      () => {
        history.replace(from);
      },
      username,
      password
    );
  };

  return (
    <div className="container py-5">
      <div className="row d-flex justify-content-center align-items-center">
        <div className="col-md-6 col-12 my-auto">
          <div className="alert alert-warning">
            <p className="mb-0">
              You must log in to view the page at {from.pathname}
            </p>
          </div>
          <div className="form-group">
            <input id="username" className="form-control" />
          </div>
          <div className="form-group">
            <input id="password" className="form-control" />
          </div>
          <div className="text-left">
            <button onClick={login} className="btn btn-primary">
              Log in
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
