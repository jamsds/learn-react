import React, { Component } from "react";

import axios from "axios";
import Contacts from "../contacts/index";

class Emails extends Component {
  state = {
    contacts: []
  };
  componentDidMount() {
    axios
      .get("http://jsonplaceholder.typicode.com/users")
      .then(res => {
        this.setState({ contacts: res.data });
      })
      .catch(console.log);
  }
  render() {
    return (
      <div className="container">
        <Contacts contacts={this.state.contacts} />
      </div>
    );
  }
}

export default Emails;
