import React, { Component } from "react";
import ReactDOM from "react-dom";

class ListContacts extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = { font: "text-dark" };
  //   this.handleClick = this.handleClick.bind(this);
  // }
  // handleClick() {
  //   console.log(ReactDOM.findDOMNode(this));
  //   this.setState({
  //     font: "text-primary"
  //   });
  // }

  componentDidMount() {
    ReactDOM.findDOMNode(this).addEventListener("click", e => {
      console.log(e.target);

      e.target.classList.toggle("text-primary");
    });
  }

  render() {
    return (
      <div className="col-md-4 col-12 mb-3">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">{this.props.name}</h5>
            <p className="card-text mb-0">{this.props.email}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default ListContacts;
