import React from "react";

import ListContacts from "./list";

const Contacts = ({ contacts }) => {
  return (
    <div className="py-4">
      <h5 className="font-weight-bold">Contacts List</h5>
      <div className="row">
        {contacts.map(contact => (
          <ListContacts
            key={contact.id}
            name={contact.name}
            email={contact.email}
          />
        ))}
      </div>
    </div>
  );
};

export default Contacts;
