import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./models/login";
import PrivateRoute from "./models/private";

import Emails from "./components/emails/index";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <PrivateRoute path="/">
            <Emails />
          </PrivateRoute>
        </Switch>
      </Router>
    );
  }
}

export default App;
